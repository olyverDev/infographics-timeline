import { isNil } from './type';

export const get = (path, object) => {
  let value = object;
  for (let i = 0; i < path.length; i += 1) {
    value = value[path[i]];

    if (isNil(value) && i !== path.length - 1) break;
  }

  return value;
};
