import translations from '../locales/en';
import { get } from './object';

export const translate = path => get(path, translations);
