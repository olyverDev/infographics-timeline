// NOTE: some dirty mocks

const getRandomPercent = () => Math.round(Math.random() * 100);
const getRandomColor = () => Math.floor(Math.random() * 16777215).toString(16);
const getCurrentYear = () => {
  const date = new Date();

  return date.getYear() + 1900;
};
const getYears = (start, end) => {
  if (start >= end) return [start];

  const years = [];

  for (let i = start; i <= end; i += 1) {
    years.push(i);
  }

  return years;
};

const LANGUAGES_IDS = ['C#', 'C', 'C++', 'Java', 'Javascript'];
const LANGUAGES = LANGUAGES_IDS.map(ID => ({ ID, color: getRandomColor() }));
const YEARS = getYears(2000, getCurrentYear());

/**
 * relations:
 * {
 *  [language]: {
 *    [year]: popularity percentage
 *  }
 * }
 */
export default {
  NAME: 'PROGRAMMING_LANGUAGES_POPULARITY',
  LABEL: 'The popularity of programming languages through years!',
  SUBJECTS: LANGUAGES,
  YEARS,
  RELATIONS: LANGUAGES.reduce(
    (acc, { ID }) => {
      acc[ID] = YEARS.reduce(
        (subjectsToPercentage, year) => {
          // it's accumulator
          // eslint-disable-next-line no-param-reassign
          subjectsToPercentage[year] = getRandomPercent();

          return subjectsToPercentage;
        },
        {},
      );

      return acc;
    },
    {},
  ),
};
